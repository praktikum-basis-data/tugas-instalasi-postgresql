# Tugas Instalasi PostgreSQL

## Bukti Instalasi
![image](https://gitlab.com/praktikum-basis-data/tugas-instalasi-postgresql/-/raw/main/ss_instal1.jpg)
![image](https://gitlab.com/praktikum-basis-data/tugas-instalasi-postgresql/-/raw/main/ss_instal2.jpg)
![image](https://gitlab.com/praktikum-basis-data/tugas-instalasi-postgresql/-/raw/main/ss_instal3.jpg)

## Bukti Konfigurasi
![image](https://gitlab.com/praktikum-basis-data/tugas-instalasi-postgresql/-/raw/main/ss_konfig1.jpg)
![image](https://gitlab.com/praktikum-basis-data/tugas-instalasi-postgresql/-/raw/main/ss_konfig2.jpg)

## Bukti Koneksi
![image](https://gitlab.com/praktikum-basis-data/tugas-instalasi-postgresql/-/raw/main/ss_konek1.jpg)
![image](https://gitlab.com/praktikum-basis-data/tugas-instalasi-postgresql/-/raw/main/ss_konek2.jpg)
